<?php

#Many thanks to Grafikart.fr 
#https://www.grafikart.fr/

namespace app\router;

class Route
{
    private $path;
    private $callable;
    private $matches = array();
    private $parameters = array();

    public function __construct($path, $callable)
    {
        $this->path = trim($path, '/');
        $this->callable = $callable;
    }

    public function match($url)
    {
        $url = trim($url, '/');
        $path = preg_replace_callback('#:([\w]+)#', [$this, 'parameter_match'], $this->path);
        $regex = "#^$path$#i";

        if (!preg_match($regex, $url, $matches)) {
            return false;
        }
        array_shift($matches);
        $this->matches = $matches;
        return true;
    }
    
    private function parameter_match($match)
    {
        if (isset($this->parameters[$match[1]])) {
            return '(' . $this->parameters[$match[1]] . ')';
        }
        return '([^/]+)';
    }

    public function call_action()
    {
        return call_user_func_array($this->callable, $this->matches);
    }
}

?>
