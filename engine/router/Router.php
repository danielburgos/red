<?php

#Many thanks to Grafikart.fr
#https://www.grafikart.fr/

namespace app\router;

class Router
{

    private $url;
    private $routes = array();

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function get_request($path, $callable)
    {
        return $this->add_route($path, $callable, 'GET');
    }

    public function post_request($path, $callable)
    {
        return $this->add_route($path, $callable, 'POST');
    }

    private function add_route($path, $callable, $method)
    {
        $route = new Route($path, $callable);
        $this->routes[$method][] = $route;
        return $route;
    }

    public function run()
    {
        if (!isset($this->routes[$_SERVER['REQUEST_METHOD']])) {
            header('', '');
        }

        foreach ($this->routes[$_SERVER['REQUEST_METHOD']] as $route) {
            if ($route->match($this->url)) {
                return $route->call_action();
            } 
        }
        throw new RouterException('This route doesn\'t exists');
    }
}

?>
