<?php

#Created by Daniel Alejandro Matute

  namespace app\database_management;

  class Oci {
    public function open_connection($user, $password, $connection_context) {
      $connection = oci_connect($user, $password, $connection_context);

      if($connection) {
        return $connection;
      }
      return NULL;
    }

    public function close_connection($connection_context) {
      oci_close($connection_context);
    }

    public function get_data($connection_context, $query) {
      $data = array();
      $statement = oci_parse($connection_context, $query);
      oci_execute($statement);
      while (($row = oci_fetch_array($statement, OCI_ASSOC))) {
        $data[] = $row;
      }
      return $data;
    }
  }
?>