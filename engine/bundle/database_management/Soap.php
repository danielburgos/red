<?php

#Created by Daniel Alejandro Matute

  namespace app\bundle\database_management;

  class Soap {
    public function get_wsdl($wsdl) {
        $soap_client = new \SoapClient($wsdl, array('cache_wsdl' => WSDL_CACHE_NONE));

        if(isset($soap_client)) {
            return $soap_client;
        }
        return NULL;
    }
  }
?>