<?php

#Created by Daniel Alejandro Matute

namespace app\bundle\collection_management;

class Collection
{
    private $items = array();

    public function add($object, $key = null)
    {
        if ($key == null) {
            $this->items[] = $object;
        } else {
            if (isset($this->items[$key])) {
                throw new KeyHasUseException("Key $key already in use.");
            } else {
                $this->items[$key] = $object;
            }
        }
    }

    public function delete($key)
    {
        if (isset($this->items[$key])) {
            unset($this->items[$key]);
    } else {
            throw new KeyInvalidException("Invalid key $key.");
        }
    }

    public function get($key) {
        if (isset($this->items[$key])) {
            return $this->items[$key];
        }
        else {
            throw new KeyInvalidException("Invalid key $key.");
        }
    }

    public function keys() {
        return array_keys($this->items);
    }

    public function length() {
        return count($this->items);
    }

    public function check($key) {
        return isset($this->items[$key]);
    }
}