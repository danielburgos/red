<?php

#Created by Daniel Alejandro Matute

namespace app\bundle\controller_management;

use app\bundle\window_management\Renderer;

class Controller extends Renderer
{
    public $router;
    public $view;
    public $route;

    public function __construct($router, $route, $view)
    {
        $this->router = $router;
        $this->view = $view;
        $this->route = $route;
        $this->self();
    }

    public function show($variables) {
        $this->render($this->view, $variables);
    }

    /*Cotroller Utilities*/

    public function expire_session($time) {
        $time = $time * 60;
        if (!isset($_SESSION['CREATED'])) {
            $_SESSION['CREATED'] = time();
        } else if (time() - $_SESSION['CREATED'] > $time) {
            session_regenerate_id(true);
            $_SESSION['CREATED'] = time();
        }
    }

    public function console_log($message) {
        echo "<script>";
        echo "console.log('" . $message . "');";
        echo "</script>";
    }

    public function print_message($message) {
        echo "<script>";
        echo "alert('" . $message . "');";
        echo "</script>";
    }

    public function split_next($character, $string) {
        return explode($character, $string);
    }

    public function get_ip_address() {
        return $_SERVER['REMOTE_ADDR'];
    }
}