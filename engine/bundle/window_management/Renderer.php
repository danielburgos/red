<?php

#Created by Daniel Alejandro Matute

namespace app\bundle\window_management;

use app;

class Renderer
{
    public function render($view, $variables)
    {
        $views = dirname(__FILE__) . '/../../../application/views';
        $loader = new \Twig_Loader_Filesystem($views);
        $twig = new \Twig_Environment($loader);

        echo $twig->render($view, $variables);
    }
}

?>