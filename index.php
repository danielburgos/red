<?php
/*Created by Daniel Alejandro Matute, 
EMail: borealink@outlook.com*/

require_once('project_settings/project_options.php');
require_once('vendor/autoload.php');

#Imports the router to the current context
$router = new app\router\Router($_GET['url']);

#Add new controllers
#-------------------------------------------------------------
$home_controller = new src\HomeController($router, '/', 'HomeView.html');
#-------------------------------------------------------------

$router->run();
?>


