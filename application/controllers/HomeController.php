<?php

namespace src;

use app\bundle\controller_management\Controller;
use app\bundle\controller_management\Activity;

class HomeController extends Controller implements Activity
{
    public function __construct($router, $route, $view)
    {
        parent::__construct($router, $route, $view);
    }

    public function self()
    {

        $variables = array(
            'CONTROLLER' => array(
                'ACTION_ROUTE' => $this->route,
                'TITLE' => 'Home'
            ),
            'CUSTOM' => array(),
            'DATABASE' => array()
        );

        #Get requests
        $this->router->get_request($this->route, function () use($variables) {
            $this->show($variables);
        });

        #Posts requests
        $this->router->post_request($this->route, function () use($variables) {
            echo 'This is post';
            $this->show($variables);
        });
    }
}